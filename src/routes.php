<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Get all cities (only id and name fields)
 */
$app->get('/', function (Request $request, Response $response) {
  $this->logger->info("Get from '/' route");

  $stmt = $this->db->query('SELECT id, name FROM cities');
  $result = $stmt->fetchAll();

  return $response->withJson($result);
});

/**
 * Get infos of a specific city
 */
$app->get('/city/{id}', function (Request $request, Response $response, $args) {
  $this->logger->info("Get from '/city/{id}' route");

  $cityId = (int)$args['id'];

  $stmt = $this->db->prepare("SELECT * FROM cities WHERE id = :id");
  $stmt->bindParam(':id', $cityId, PDO::PARAM_INT);
  $stmt->execute();

  if ($result = $stmt->fetch()) {
    return $response->withJson($result);
  } 

  return $response->withStatus(404);
});

/**
 * Add a new city
 * TODO: make a stronger control of parameters with more meaningful response errors
 * 
 * Example with cUrl : 
 * curl -H "Content-Type: application/json" -X POST -d '{"name": "Bordeaux", "zipcode": 33000}' http://localhost:8080/city
 */
$app->post('/city', function (Request $request, Response $response, $args) {
  $this->logger->info("Post from '/city' route");

  $data = $request->getParsedBody();

  $stmt = $this->db->prepare("INSERT INTO cities (name, zipcode) VALUES (:name, :zipcode)");
  $stmt->bindParam(':name', $cityId, PDO::PARAM_STR);
  $stmt->bindParam(':zipcode', $cityZipcode, PDO::PARAM_INT);

  $cityId = filter_var($data['name'], FILTER_SANITIZE_STRING);
  $cityZipcode = filter_var($data['zipcode'], FILTER_VALIDATE_INT);

  if ($stmt->execute()) {
    return $response->withStatus(201);
  }

  return $response->withStatus(500);
});
