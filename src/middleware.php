<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->add(function (Request $request, Response $response, callable $next) {
  $response = $next($request, $response);
  return $response->withAddedHeader('Content-type', 'application/json');
});
