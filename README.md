# Simple Slim App example

## Install dependencies

```bash
composer install
```

## Documentation

[Slim Framework](http://www.slimframework.com/docs/)
[PHP PDO](http://php.net/manual/fr/book.pdo.php)
